<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link text-center">
        <span class="brand-text font-weight-light">Forum Diskusi</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        @auth
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('admin/dist/img/user.png') }}" class="img-circle elevation-2"
                    alt="User Image">
            </div>
            <div class="info">
                <a href="/profil" class="d-block">{{ Auth::user()->username }}</a>
            </div>
        </div>
        @endauth
        {{-- <div class="info">
            @auth
                <a href="#" class="d-block">{{ Auth::user()->username }}</a>
            @endauth
        </div> --}}
    </div>

    <!-- SidebarSearch Form -->
    <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
            <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
                <button class="btn btn-sidebar">
                    <i class="fas fa-search fa-fw"></i>
                </button>
            </div>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item">
                <a href="/" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        Dashboard
                    </p>
                </a>
            </li>

            @auth
                <li class="nav-item">
                    <a href="/pertanyaan/" class="nav-link">
                        <i class="nav-icon fa fa-question-circle"></i>
                        <p>
                            Pertanyaan
                        </p>
                    </a>
                </li>
            @endauth

             @auth
                <li class="nav-item">
                    <a href="/profil" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Profil
                        </p>
                    </a>
                </li>
            @endauth

            @auth
                <li class="nav-item">
                    <a href="/kategori" class="nav-link">
                        <i class="nav-icon fas fa-table"></i>
                        <p>
                            Kategori
                        </p>
                    </a>
                </li>
            @endauth

            <!-- Tombol Logout -->
            {{-- Logout --}}

            <li class="nav-item">
                {{-- <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();"> --}}
                    <!-- Tombol Logout -->
                    {{-- Logout --}}
                    @auth
                <li class="nav-item bg-danger">
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                      <i class="bi bi-box-arrow-right"></i> Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none">
                        @csrf
                    </form>
                </li>
            @endauth
            {{-- penutup Logout --}}

            <!-- Jika belum login -->
            @guest
                <li class="nav-item bg-primary">
                    <a href="/login" class="nav-link">
                        <p>
                            Login
                        </p>
                    </a>
                </li>
            @endguest
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
    {{-- </div> --}}
    <!-- /.sidebar -->
</aside>
