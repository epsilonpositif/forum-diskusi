@extends('layout.master')
@section('judul')
    Pertanyaan
@endsection


@section('content')
    <div class="font-weight-bold">{!! $pertanyaan->isi_pertanyaan !!}</div>
    <img src="{{ asset('aset_gambar/' . $pertanyaan->gambar) }}" style="width:300px;height:300px;">
    <hr>
    <div class="card p-2">
        <div class="my-2">Beri tanggapan</div>
        <form action="/jawaban" method="POST">
            @csrf
            <input type="hidden" name="pertanyaan_id" value="{{$pertanyaan->id}}">
            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
            <textarea name="isi_jawaban" class="form-control" id="" cols="30" rows="5"></textarea>
            <button class="btn btn-primary my-2 btn-sm">Kirim tanggapan</button>
        </form>
    </div>
    @foreach ($pertanyaan->jawaban as $item)
        <div class="alert alert-light" style="background-color: #E3E3FF">
            {{ $item->isi_jawaban }}
            <hr style="border-bottom: solid 1px grey">
            <div>
                <small>
                    <i class="mr-3">oleh : {{ $item->user->username }}</i>
                    @if (Auth::user()->username == $item->user->username)
                        <form action="/jawaban/{{ $item->id }}" method="POST" class="d-inline">
                            <a class="btn btn-sm" href="/jawaban/{{ $item->id }}/edit"
                                style="text-decoration: none;color:blueviolet"><i class="bi bi-pencil-square"></i> edit</a>
                            @csrf
                            @method('delete')
                            <button class="btn btn-sm text-danger" style="text-decoration: none;"><i
                                    class="bi bi-pencil-square"></i> Hapus</button>
                        </form>
                    @endif
                </small>

            </div>

        </div>
    @endforeach
    <hr>
    {{-- <a href="/pertanyaan" class="btn btn-info my-4">Kembali</a> --}}
@endsection
