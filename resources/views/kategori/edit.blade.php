@extends('layout.master')
@section('judul')
    Edit Kategori
@endsection
@push('script')
    <script src="https://cdn.tiny.cloud/1/1du3ha6o8wslmz5o846ttnrw5aazh3746rb6epx58mq88klj/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
            toolbar: 'undo redo | formatselect | ' +
                'bold italic backcolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | ' +
                'removeformat | help',
            // toolbar_mode: 'floating',
            tinycomments_mode: 'embedded',
            tinycomments_author: 'Author name',
        });
    </script>
@endpush
@section('content')
    <form method="POST" action="/kategori/{{$kategori->id}}">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="exampleInputEmail1">Nama</label>
            <input type="text" class="form-control" name="nama" id="exampleInputEmail1" value="{{$kategori->nama}}" aria-describedby="emailHelp">
            @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Deskripsi</label>
            <textarea name="deskripsi" class="form-control" cols="30" rows="10">{{$kategori->deskripsi}}</textarea>
            {{-- <input type="text" class="form-control" name="umur" value="{{$kategori->deskripsi}}" id="exampleInputPassword1"> --}}
            @error('deskripsi')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
       
        <a href="/kategori" class="btn btn-danger">Kembali</a>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
