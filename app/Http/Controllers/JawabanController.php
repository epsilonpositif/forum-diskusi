<?php

namespace App\Http\Controllers;
use DB;
use App\Jawaban;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert; //sweet alert


class JawabanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
  

    public function create( $id)
    {
        // return view('jawaban.create', compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'isi_jawaban' => 'required',
            'user_id' => 'required',
            'pertanyaan_id' => 'required',
        ]);


        $jawaban = new Jawaban;

        $jawaban->isi_jawaban = $request->isi_jawaban;
        $jawaban->user_id = $request->user_id;
        $jawaban->pertanyaan_id = $request->pertanyaan_id;


        $jawaban->save();
        Alert::success('Berhasil', 'Jawaban berhasil disimpan');
        return redirect('/pertanyaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $jawaban=Jawaban::findOrFail($id);

         return view('jawaban.edit', compact('jawaban'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'isi_jawaban' => 'required'
        ]);

        $jawaban= Jawaban::find($id);
        $jawaban->isi_jawaban = $request->isi_jawaban;
        $jawaban->update();
        Alert::success('Berhasil', 'Jawaban berhasil diperbarui');
        return redirect('/pertanyaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jawaban = Jawaban::find($id);
        $jawaban->delete();
        Alert::info('Dihapus', 'Jawaban anda berhasil dihapus');
        return redirect('/pertanyaan');
    }
}
