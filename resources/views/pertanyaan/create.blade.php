@extends('layout.master')
@section('judul')
    Buat Pertanyaanmu
@endsection
@push('script')
    <script src="https://cdn.tiny.cloud/1/1du3ha6o8wslmz5o846ttnrw5aazh3746rb6epx58mq88klj/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
            toolbar: 'undo redo | formatselect | ' +
                'bold italic backcolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | ' +
                'removeformat | help',
            // toolbar_mode: 'floating',
            tinycomments_mode: 'embedded',
            tinycomments_author: 'Author name',
        });
    </script>
@endpush
@section('content')
    <div class="container">
        <form method="POST" action="/pertanyaan" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>User ID</label>
                <input readonly type="text" value="{{ Auth::user()->id }}" class="form-control" name="user_id">
            </div>
            @error('user_id')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="form-group">
                <label>Isi Pertanyaan</label>
                <textarea name="isi_pertanyaan" class="form-control" placeholder="Isi pertanyaan"></textarea>
            </div>
            @error('isi_pertanyaan')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
    {{-- </div> --}}
    <div class="form-group">
        <label>Kategori</label>
        <select name="kategori_id" class="form-control">
            <option value="">Pilih Kategori</option>
            @foreach ($kategori as $item)
                <option value="{{ $item->id }}">{{ $item->nama }}</option>
            @endforeach
        </select>
        @error('kategori_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label>Upload Gambar</label>
        <input type="file" class="form-control" name="gambar">
    </div>
    @error('gambar')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <a href="/pertanyaan" class="btn btn-danger">Kembali</a>
    <button type="submit" class="btn btn-primary">Submit</button>

    </form>
    </div>
@endsection
