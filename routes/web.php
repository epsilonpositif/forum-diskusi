<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'indexController@index');

//CRUD User------------------------------------------------------------------
//create
Route::get('/user/create', 'UserController@create');
Route::post('/user', 'UserController@store');

//read
Route::get('/user', 'UserController@index');
Route::get('/user/{user_id}', 'UserController@show');

//update
Route::get('/user/{user_id}/edit', 'UserController@edit');
Route::put('/user/{user_id}', 'UserController@update');

//delete
Route::delete('/user/{user_id}', 'UserController@destroy');

//Diarahkan ke login dulu
Route::group(['middleware' => ['auth']], function () {
//Profil--------------------------------------------------------------------
Route::resource('profil','ProfilController')->only([
'index','update']);
});


//CRUD Kategori-----------------------------------------------------------------------
//create
Route::get('/kategori/create', 'KategoriController@create');
Route::post('/kategori', 'KategoriController@store');

//read
Route::get('/kategori', 'KategoriController@index');
Route::get('/kategori/{kategori_id}', 'KategoriController@show');

//update
Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit');
Route::put('/kategori/{kategori_id}', 'KategoriController@update');

//delete
Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy');

//CRUD Pertanyaan-----------------------------------------------------------------------
Route::group(['middleware' => ['auth']], function () {
//create
Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/pertanyaan', 'PertanyaanController@store');

//read
Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@show');

//update
Route::get('/pertanyaan/edit/{pertanyaan_id}/', 'PertanyaanController@edit');
// Route::get('/pertanyaan/jawab/{pertanyaan_id}/', 'PertanyaanController@jawab');
Route::put('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');

//delete
Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy');
});

//CRUD Jawaban-----------------------------------------------------------------------
//create
Route::get('/jawaban/create', 'JawabanController@create');
Route::post('/jawaban', 'JawabanController@store');

//read
Route::get('/jawaban', 'JawabanController@index');
Route::get('/jawaban/{jawaban_id}', 'JawabanController@show');

//update
Route::get('/jawaban/{jawaban_id}/edit', 'JawabanController@edit');
Route::put('/jawaban/{jawaban_id}', 'JawabanController@update');

//delete
Route::delete('/jawaban/{jawaban_id}', 'JawabanController@destroy');

Auth::routes();

