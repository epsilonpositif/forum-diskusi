@extends('layout.master')


@push('script')
<script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#tbl_kategori").DataTable();
  });
</script>
@endpush

@push('csstabel')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush


@section('judul')
    Data Kategori
@endsection

@section('content')
    <a href="/kategori/create" class="btn btn-primary my-4">Tambahkan kategori</a>
    <table id="tbl_kategori" class="table">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Nama kategori</th>
                <th scope="col">Deskripsi</th>
                <th scope="col">Pertanyaan</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($kategori as $key=> $item)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ strip_tags($item->deskripsi) }}</td>
                    <td>
                       <ul>
                        @foreach ($item->pertanyaan as $value)
                            <li>{{strip_tags($value->isi_pertanyaan)}}</li>
                        @endforeach
                       </ul>
                    </td>
                    <td>
                        
                        <form action="/kategori/{{$item->id}}" method="post">
                            {{-- <a href="/cast/{{$item->id}}" class="btn btn-success btn-sm">Detail</a> --}}
                        <a href="/kategori/{{$item->id}}/edit" class="btn btn-info btn-sm">Edit</a>
                        @csrf
                        @method('delete')
                        <button class="btn btn-danger btn-sm" type="submit" name="delete">Hapus</button>
                        </form>
                    </td>

                </tr>

            @empty
                {{-- <div>Data tidak tersedia</div> --}}
            @endforelse
        </tbody>
    </table>
@endsection
