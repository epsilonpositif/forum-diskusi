@extends('layout.master')
@section('judul')
    Edit jawaban
@endsection

@section('content')
    <form method="POST" action="/jawaban/{{$jawaban->id}}">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="exampleInputPassword1">Deskripsi</label>
            <textarea name="isi_jawaban" class="form-control" cols="30" rows="10">{!!$jawaban->isi_jawaban!!}</textarea>
            {{-- <input type="text" class="form-control" name="umur" value="{{$kategori->deskripsi}}" id="exampleInputPassword1"> --}}
            @error('isi_jawaban')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
       
        <button type="submit" class="btn btn-primary">Kirim</button>
    </form>
@endsection
