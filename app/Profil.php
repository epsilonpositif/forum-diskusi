<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $table="profil";
    protected $fillable=["nama_lengkap","umur","alamat","user_id"];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
}
