@extends('layout.master')

@section('judul')
    Profil Anda
@endsection


@push('script')
    <script src="https://cdn.tiny.cloud/1/1du3ha6o8wslmz5o846ttnrw5aazh3746rb6epx58mq88klj/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
            toolbar: 'undo redo | formatselect | ' +
                'bold italic backcolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | ' +
                'removeformat | help',
            // toolbar_mode: 'floating',
            tinycomments_mode: 'embedded',
            tinycomments_author: 'Author name',
        });
    </script>
@endpush


@section('content')
    <form method="POST" action="/profil/{{ $profil->id }}">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Email</label>
            <input type="text" value="{{ $profil->user->email }}" class="form-control" disabled>
        </div>
        <div class="form-group">
            <label>Username</label>
            <input type="text" value="{{ $profil->user->username }}" class="form-control" disabled>
        </div>

        <div class="form-group">
            <label>Nama Lengkap</label>
            <input type="text" name="nama_lengkap" value="{{ $profil->nama_lengkap }}" class="form-control">
        </div>
        @error('nama_lengkap')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Umur</label>
            <input type="text" name="umur" value="{{ $profil->umur }}" class="form-control">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Alamat</label>
            <textarea name="alamat" class="form-control">{{ $profil->alamat }}</textarea>
        </div>
        @error('alamat')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
@endsection
