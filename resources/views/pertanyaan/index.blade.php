@extends('layout.master')
@section('judul')
<h3>Daftar pertanyaan</h3>
@endsection

@section('content')

    {{-- <div class="container"> --}}
    <div class="container">
        
        {{-- <br> --}}
        <div class="container">

            <a href="/pertanyaan/create" class="btn btn-primary mb-3">+ Buat Pertanyaan</a>
            <div>
                {{-- <h4>Daftar pertanyaan</h4><br> --}}
                @forelse($pertanyaan as $key=>$item)
                    <div class="card shadow p-3">
                        <div class="bg-secondary p-2" style="border-radius: 10px">
                            <a href="/pertanyaan/{{ $item->id }}" class="mr-3"><i class="bi bi-eye"></i>
                                Lihat utas</a>
                            {{-- <a href="/jawaban/{{ $item->id }}" class="mr-3"><i class="bi bi-chat-right"></i>
                                Beri tanggapan</a> --}}
                            @if (Auth::user()->username == $item->user->username)
                                <a href="/pertanyaan/edit/{{ $item->id }}" class="mr-3 btn btn-success btn-sm"><i
                                        class="bi bi-pencil-square"></i>
                                    Edit</a>
                                <form action="/pertanyaan/{{ $item->id }}" method="POST" class="d-inline">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger btn-sm" name="delete-ask"
                                        class="mr-3"><i class="bi bi-trash"></i>
                                        Delete</button>
                                </form>
                            @endif
                        </div>
                        {{-- <h5>{{ $item->user->username }}</h5> --}}
                        {{-- <div><i><small>Dikirim : {{ $item->created_at }} | Kategori : {{ $item->kategori->nama }}</small>
                            </i></div> --}}
                        <hr>
                        <div>{{ strip_tags($item->isi_pertanyaan) }}</div>
                        <div>
                            <img src="" style="max-width: 100px;" alt="">
                        </div>
                        {{-- <div>Kategori : {{ $item->kategori->nama }}</div> --}}
                        <hr>
                        <div><i><small>Dikirim oleh <b>{{ $item->user->username }}</b> pada {{ $item->created_at }}
                                    |
                                    Kategori : {{ $item->kategori->nama }}</small>
                            </i></div>
                    </div>
                @empty
                    <h1>Data Kosong</h1>
                @endforelse
            </div>
        </div>
    </div>
@endsection
