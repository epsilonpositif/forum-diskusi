<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table="pertanyaan";
    protected $fillable=["isi_pertanyaan","user_id","kategori_id","gambar"];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function jawaban(){
        return $this->hasMany('App\Jawaban');
    }

    public function kategori(){
        return $this->belongsTo('App\Kategori','kategori_id');
    }
}