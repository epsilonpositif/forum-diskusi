<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pertanyaan;

use DB;


use RealRashid\SweetAlert\Facades\Alert; //sweet alert



class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pertanyaan=Pertanyaan::get();
        return view('pertanyaan.index',compact('pertanyaan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori=DB::table('kategori')->get();
        return view('pertanyaan.create',compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'isi_pertanyaan' => 'required',
            'user_id' => 'required',
            'kategori_id' => 'required',
            'gambar'=>'image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $gambarName=time().'.'.$request->gambar->extension();
        $request->gambar->move(public_path('aset_gambar'),$gambarName);

        $pertanyaan = new Pertanyaan;

        $pertanyaan->isi_pertanyaan = $request->isi_pertanyaan;
        $pertanyaan->user_id = $request->user_id;
        $pertanyaan->kategori_id = $request->kategori_id;
        $pertanyaan->gambar = $gambarName;


        $pertanyaan->save();
        Alert::success('Berhasil', 'Pertanyaan berhasil ditambahkan');
        return redirect('/pertanyaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        // $jawaban = Jawaban::where('pertanyaan_id', $id);
        return view('pertanyaan.show', compact('pertanyaan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $kategori=DB::table('kategori')->get();
         $pertanyaan=Pertanyaan::findOrFail($id);

         return view('pertanyaan.edit', compact('pertanyaan','kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'isi_pertanyaan' => 'required',
            'user_id' => 'required',
            'kategori_id' => 'required',
            'gambar'=>'image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $pertanyaan= Pertanyaan::find($id);
        if ($request->has('gambar')) {
            $gambarName=time().'.'.$request->gambar->extension();
            $request->gambar->move(public_path('aset_gambar'),$gambarName);

            $pertanyaan->isi_pertanyaan = $request->isi_pertanyaan;
            $pertanyaan->user_id = $request->user_id;
            $pertanyaan->kategori_id = $request->kategori_id;
            $pertanyaan->gambar = $gambarName;
        }else{
            $pertanyaan->isi_pertanyaan = $request->isi_pertanyaan;
            $pertanyaan->user_id = $request->user_id;
            $pertanyaan->kategori_id = $request->kategori_id;
        }
        $pertanyaan->update();
        return redirect('/pertanyaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pertanyaan = Pertanyaan::find($id);

        $pertanyaan->delete();
        Alert::info('Dihapus', 'Pertanyaan berhasil dihapus');
        return redirect('/pertanyaan');
    }
}
