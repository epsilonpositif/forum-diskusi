@extends('layout.master')
@section('judul')
 Edit Pertanyaanmu
@endsection

@section('content')
<div class="container">
<form method="POST" action="/pertanyaan/{{$pertanyaan->id}}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group"> 
          <label>User ID</label>
          <input readonly type="text" value="{{Auth::user()->id}}" class="form-control" name="user_id">
          </div>
          @error('user_id')
              <div class="alert alert-danger">{{$message}}</div>
           @enderror
        <div class="form-group">
            <label>Isi Pertanyaan</label>
            <textarea name="isi_pertanyaan" class="form-control">{{$pertanyaan->isi_pertanyaan}}</textarea>
            </div>
            @error('isi_pertanyaan')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Kategori</label>
            <select name="kategori_id" class="form-control">
                <option value="">Pilih Kategori</option>
                @foreach ($kategori as $item)
                @if ($item->id===$pertanyaan->kategori_id)
                    <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                @else
                     <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                @endif
                @endforeach
            </select>
            @error('kategori_id')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group"> 
          <label>Upload Gambar</label>
          <input type="file" class="form-control" name="gambar">
          </div>
          @error('gambar')
              <div class="alert alert-danger">{{$message}}</div>
           @enderror

        <a href="/pertanyaan" class="btn btn-danger">Kembali</a>
        <button type="submit" class="btn btn-primary">Simpan</button>

    </form>
</div>
@endsection