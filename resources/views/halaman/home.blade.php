@extends('layout.master')
@section('judul')
Forum Diskusi
@endsection

@section('content')
<!-- <h3>Learn together, build together</h3>
<div>
    Mari saling berbagi informasi, tanyakan apapun yang kamu ingin tau.
</div>
<img src="{{ asset('admin/dist/img/disscus.png') }}"> -->
<div class="card text-center">
  <div class="card-header">
    <h3>Learn Together, Build Together</h3>
  </div>
  <div class="text-center">
  <img class="card-img-top" src="{{ asset('admin/dist/img/discuss.png') }}" alt="Card image cap" style="width:300px;height:300px;">
  </div>
  <div class="card-body">
    <h5 class="card-text">Mari saling berbagi informasi, tanyakan apapun yang kamu ingin tau.</h5>
    <a href="/register" class="btn btn-primary">Bergabung Bersama Kami</a>
  </div>
  <div class="card-footer text-muted">
    Since 2022
  </div>
</div>
@endsection
 
    
